package com.amigoscode.streams;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.amigoscode.streams._Stream.Gender.*;

public class _Stream {
    public static void main(String[] args) {

        final List<Person> people = List.of(
                new Person("John", MALE),
                new Person("Maria", FEMALE),
                new Person("Aisha", FEMALE),
                new Person("Alex", MALE),
                new Person("Alice", FEMALE),
                new Person("Bob", NOT_SAY)
        );

        System.out.println("// Genders");
        people.stream()
                .map(Person::getGender)
                .collect(Collectors.toSet())
                .forEach(System.out::println);

        System.out.println("// Names");
        people.stream()
                .map(Person::getName)
                .collect(Collectors.toSet())
                .forEach(System.out::println);

        System.out.println("// Names length");
        people.stream()
                .map(Person::getName)
                .mapToInt(String::length)
                .forEach(System.out::println);

        System.out.println("// Predicate");
        final Predicate<Person> isFemale = person -> FEMALE.equals(person.getGender());

        final boolean containsOnlyFemales = people.stream()
                .allMatch(isFemale);

        System.out.println("containsOnlyFemales = " + containsOnlyFemales);

        final boolean containsAnyFemale = people.stream()
                .anyMatch(isFemale);
        System.out.println("containsAnyFemale = " + containsAnyFemale);

        final boolean containsNoneFemale = people.stream()
                .noneMatch(isFemale);
        System.out.println("containsNoneFemale = " + containsNoneFemale);

    }

    static class Person {
        private final String name;
        private final Gender gender;

        public Person(String name, Gender gender) {
            this.name = name;
            this.gender = gender;
        }

        public String getName() {
            return name;
        }

        public Gender getGender() {
            return gender;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", gender=" + gender +
                    '}';
        }
    }

    enum Gender {
        MALE, FEMALE, NOT_SAY
    }

}
