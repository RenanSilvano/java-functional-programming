package com.amigoscode.functionalinterface;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class _Consumer {
    public static void main(String[] args) {
        final Custumer maria = new Custumer("Maria", "999999999");
        // Normal function
        System.out.println("// Normal function");
        greetCustomer(maria);

        // Consumer Functional Interface
        System.out.println("// Consumer Functional Interface");
        greetCustumerConsumer.accept(maria);

        // ByConsumer Functional Interface
        System.out.println("// ByConsumer Functional Interface");
        greetCustumerBiConsumer.accept(maria, true);
        greetCustumerBiConsumer.accept(maria, false);
    }

    static void greetCustomer(final Custumer custumer) {
        System.out.println("Hello " + custumer.getName() + ", thanks for registering your phone number " + custumer.getPhoneNumber());
    }

    static Consumer<Custumer> greetCustumerConsumer =
            custumer -> System.out.println("Hello " + custumer.getName() + ", thanks for registering your phone number " + custumer.getPhoneNumber());

    static BiConsumer<Custumer, Boolean> greetCustumerBiConsumer =
            (custumer, showPhoneNumber) -> System.out.println("Hello " + custumer.getName() +
                    ", thanks for registering your phone number " +
                    (showPhoneNumber ? custumer.getPhoneNumber() : "********"));

    static class Custumer {
        private final String name;
        private final String phoneNumber;

        public Custumer(String name, String phoneNumber) {
            this.name = name;
            this.phoneNumber = phoneNumber;
        }

        public String getName() {
            return name;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }
    }
}
