package com.amigoscode.functionalinterface;

import java.util.function.Predicate;

public class _Predicate {
    public static void main(String[] args) {
        // Normal function
        System.out.println("// Normal function");
        System.out.println("isPhoneNumberValid(\"07000000000\") = " + isPhoneNumberValid("07000000000"));
        System.out.println("isPhoneNumberValid(null) = " + isPhoneNumberValid(null));
        System.out.println("isPhoneNumberValid(\"0700000\") = " + isPhoneNumberValid("0700000"));
        System.out.println("isPhoneNumberValid(\"09000007891\") = " + isPhoneNumberValid("09000007891"));

        // Predicate Functional Interface
        System.out.println("// Predicate Functional Interface");
        System.out.println("isPhoneNumberValidPredicate.test(\"07000000000\") = " + isPhoneNumberValidPredicate.test("07000000000"));
        System.out.println("isPhoneNumberValidPredicate.test(null) = " + isPhoneNumberValidPredicate.test(null));
        System.out.println("isPhoneNumberValidPredicate.test(\"0700000\") = " + isPhoneNumberValidPredicate.test("0700000"));
        System.out.println("isPhoneNumberValidPredicate.test(\"09000007891\") = " + isPhoneNumberValidPredicate.test("09000007891"));

        final Predicate<String> isPhoneNumberValidAndContainsNumber3Predicate = isPhoneNumberValidPredicate.and(contaisNumber3);

        System.out.println("isPhoneNumberValidAndContainsNumber3Predicate.test(\"07000000000\") = " + isPhoneNumberValidAndContainsNumber3Predicate.test("07000000000"));
        System.out.println("isPhoneNumberValidAndContainsNumber3Predicate.test(\"07000000003\") = " + isPhoneNumberValidAndContainsNumber3Predicate.test("07000000003"));

        final Predicate<String> isPhoneNumberValidOrContainsNumber3Predicate = isPhoneNumberValidPredicate.or(contaisNumber3);
        System.out.println("isPhoneNumberValidOrContainsNumber3Predicate.test(\"07000000000\") = " + isPhoneNumberValidOrContainsNumber3Predicate.test("07000000000"));
        System.out.println("isPhoneNumberValidOrContainsNumber3Predicate.test(\"03000000000\") = " + isPhoneNumberValidOrContainsNumber3Predicate.test("03000000000"));

    }

    static boolean isPhoneNumberValid(final String phoneNumber) {
        return phoneNumber != null
                && phoneNumber.startsWith("07")
                && phoneNumber.length() == 11;
    }

    static Predicate<String> isPhoneNumberValidPredicate =
            phoneNumber -> phoneNumber != null
                    && phoneNumber.startsWith("07")
                    && phoneNumber.length() == 11;

    static Predicate<String> contaisNumber3 = phoneNumber -> phoneNumber.contains("3");

}
