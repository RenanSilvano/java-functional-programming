package com.amigoscode.functionalinterface;

import java.util.List;
import java.util.function.Supplier;

public class _Supplier {
    public static void main(String[] args) {
        System.out.println("// Normal function");
        System.out.println("getDBConnectionUrl() = " + getDBConnectionUrl());

        System.out.println("// Supplier functional interface");
        System.out.println("getDBConnectionUrlSupplier.get() = " + getDBConnectionUrlSupplier.get());
        System.out.println("getDBConnectionUrlsSupplier.get() = " + getDBConnectionUrlsSupplier.get());
    }

    static String getDBConnectionUrl() {
        return "jdbc://localhost:5432/users";
    }

    static Supplier<String> getDBConnectionUrlSupplier =  () -> "jdbc://localhost:5432/users";
    static Supplier<List<String>> getDBConnectionUrlsSupplier =  () -> List.of("jdbc://localhost:5432/users", "jdbc://localhost:5432/custumers");
}
