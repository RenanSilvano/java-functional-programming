package com.amigoscode.functionalinterface;

import java.util.function.BiFunction;
import java.util.function.Function;

public class _Function {
    public static void main(String[] args) {
        // Function Interface
        System.out.println("// Function Interface");
        final int increment = incrementByOne(0);
        System.out.println("increment = " + increment);

        final int increment2 = incrementByOneFunction.apply(1);
        System.out.println("increment2 = " + increment2);

        final int multiply = multiplyBy10Function.apply(increment2);
        System.out.println("multiply = " + multiply);

        final Function<Integer, Integer> addBy1AndThenMultiplyBy10 =
                incrementByOneFunction.andThen(multiplyBy10Function);
        final int addedAndMultiplied = addBy1AndThenMultiplyBy10.apply(4);
        System.out.println("addedAndMultiplied = " + addedAndMultiplied);

        // BiFunction Interface
        System.out.println("// BiFunction Interface");
        final int incrementedByOneAndMultipliedBy = incrementByOneAndMultiplyBy(4,100);
        System.out.println("incrementedByOneAndMultipliedBy = " + incrementedByOneAndMultipliedBy);

        final int incrementedByOneAndMultipliedByBiFunction = incrementByOneAndMultiplyByBiFunction.apply(4,100);
        System.out.println("incrementedByOneAndMultipliedByBiFunction = " + incrementedByOneAndMultipliedByBiFunction);
        System.out.println("incrementedByOneAndMultipliedByBiFunction = " + incrementedByOneAndMultipliedByBiFunction);

    }

    final static Function<Integer, Integer> incrementByOneFunction = (number) -> number + 1;

    final static Function<Integer, Integer> multiplyBy10Function = (number) -> number * 10;

    static int incrementByOne(int number) {
        return number + 1;
    }

    final static BiFunction<Integer, Integer, Integer> incrementByOneAndMultiplyByBiFunction = (number, multiplyBy) -> (number + 1) * multiplyBy;

    static int incrementByOneAndMultiplyBy(int number, int mumToMultiplyBy) {
        return (number + 1) * mumToMultiplyBy;
    }

}
