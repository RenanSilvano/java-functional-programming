package com.amigoscode.optionals;

import java.util.Optional;

public class _Optionals {
    public static void main(String[] args) {
        final Object value = Optional.ofNullable(null)
                .orElseGet(() -> "default value");

        System.out.println("value = " + value);


        Optional.ofNullable("john@gmail.com")
                .ifPresent(System.out::println);

        Optional.ofNullable("john@gmail.com")
                .ifPresent(email -> System.out.println("Send email to: " + email));

        Optional.ofNullable(null)
                .ifPresentOrElse(
                        email -> System.out.println("Send email to: " + email),
                        () -> System.out.println("Cannot send email")
                );
    }
}
