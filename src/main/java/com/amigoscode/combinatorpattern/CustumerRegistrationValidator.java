package com.amigoscode.combinatorpattern;

import java.time.LocalDate;
import java.time.Period;
import java.util.function.Function;

import static com.amigoscode.combinatorpattern.CustumerRegistrationValidator.ValidationResult;

public interface CustumerRegistrationValidator extends Function<Custumer, ValidationResult> {

    static CustumerRegistrationValidator isEmailValid() {
        return custumer -> custumer.getEmail() != null && custumer.getEmail().contains("@")
                ? ValidationResult.SUCCESS : ValidationResult.EMAIL_NOT_VALID;
    }

    static CustumerRegistrationValidator isPhoneNumberValid() {
        return custumer -> custumer.getPhoneNumber() != null && custumer.getPhoneNumber().startsWith("+0")
                ? ValidationResult.SUCCESS : ValidationResult.PHONE_NUMBER_NOT_VALID;
    }

    static CustumerRegistrationValidator isAdult() {
        return custumer -> Period.between(custumer.getDob(), LocalDate.now()).getYears() > 18
                ? ValidationResult.SUCCESS : ValidationResult.IS_NOT_AN_ADULT;
    }

    default CustumerRegistrationValidator and(CustumerRegistrationValidator other) {
        return custumer -> {
            final ValidationResult result = this.apply(custumer);
            return result.equals(ValidationResult.SUCCESS) ? other.apply(custumer) : result;
        };
    }


    enum ValidationResult {
        SUCCESS,
        PHONE_NUMBER_NOT_VALID,
        EMAIL_NOT_VALID,
        IS_NOT_AN_ADULT
    }
}
