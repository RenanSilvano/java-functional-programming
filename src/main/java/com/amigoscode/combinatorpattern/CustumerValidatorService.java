package com.amigoscode.combinatorpattern;

import java.time.LocalDate;
import java.time.Period;

public class CustumerValidatorService {

    public boolean isValid(final Custumer custumer) {
        return isEmailValid(custumer.getEmail())
                && isPhoneNumberValid(custumer.getPhoneNumber())
                && isAdult(custumer.getDob());
    }

    private boolean isEmailValid(final String email) {
        return email != null && email.contains("@");
    }

    private boolean isPhoneNumberValid(final String phoneNumber) {
        return phoneNumber != null && phoneNumber.startsWith("+0");
    }

    private boolean isAdult(final LocalDate dob) {
        return Period.between(dob, LocalDate.now()).getYears() > 18;
    }

}
