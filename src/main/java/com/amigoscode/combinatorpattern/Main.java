package com.amigoscode.combinatorpattern;

import java.time.LocalDate;

import static com.amigoscode.combinatorpattern.CustumerRegistrationValidator.*;

public class Main {
    public static void main(String[] args) {
        // Normal
        System.out.println("// Using combinator pattern");
        final Custumer custumer = new Custumer(
                "Alice",
                "alice@gmail.com",
                "+0897045528",
                LocalDate.of(2000, 1, 1)
        );

        final CustumerValidatorService validatorService = new CustumerValidatorService();
        System.out.println("validatorService.isValid(custumer) = " + validatorService.isValid(custumer));


        // Using combinator pattern
        System.out.println("// Using combinator pattern");
        final ValidationResult result = isEmailValid()
                .and(isPhoneNumberValid())
                .and(isAdult())
                .apply(custumer);

        System.out.println("result = " + result);

        if (!ValidationResult.SUCCESS.equals(result)) {
            throw new IllegalStateException(result.name());
        }

    }
}
