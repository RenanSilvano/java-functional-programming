package com.amigoscode.finalsection;

import java.util.function.Consumer;

public class Callbacks {
    public static void main(String[] args) {

        hello("John", null, value -> System.out.println("No last name provided for " + value));
        hello2("John", null, () -> System.out.println("No last name provided"));

    }

    static void hello(String firstName, String lastName, Consumer<String> callback) {
        System.out.println("firstName = " + firstName);
        if (lastName != null) {
            System.out.println("lastName = " + lastName);
        } else {
            callback.accept(firstName);
        }
    }

    static void hello2(String firstName, String lastName, Runnable callback) {
        System.out.println("firstName = " + firstName);
        if (lastName != null) {
            System.out.println("lastName = " + lastName);
        } else {
            callback.run();
        }
    }
}
