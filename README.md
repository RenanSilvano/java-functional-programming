# Java Functional Programming

[Java Functional Programming | Full Course | 2020](https://www.youtube.com/watch?v=VRpHdSFWGPs)
from [AmigosCode Youtube channel](https://www.youtube.com/channel/UC2KfmYEM4KCuA1ZurravgYw)

## Functions

Describes an interface that receive an ```Object``` and returns another ```Object```.
```java
Function<Integer, Integer> incrementByOneFunction = number -> number + 1;
System.out.println(incrementByOneFunction.apply(1)); // 2
```
Check this code for some exemples [_Function.java](/src/main/java/com/amigoscode/functionalinterface/_Function.java) or see the video at [this point](https://youtu.be/VRpHdSFWGPs?t=1376)

## BiFunctions

Describes an interface that receive two ```Object``` and return another one ```Object```.
```java
BiFunction<Integer, Integer, Integer> incrementByOneAndMultiplyByBiFunction = (number, multiplyBy) -> (number + 1) * multiplyBy;
System.out.println(incrementByOneAndMultiplyByBiFunction.apply(4, 100)) // 500;
```

Check this code for some exemples [_Function.java](/src/main/java/com/amigoscode/functionalinterface/_Function.java) or see the video at [this point](https://youtu.be/VRpHdSFWGPs?t=2165)

## Consumers

Describes an interface that receive an ```Object``` and do anything with this.
```java
Consumer<Custumer> greetCustumerConsumer =
    custumer -> System.out.println("Hello " + custumer.getName() + ", thanks for registering your phone number " + custumer.getPhoneNumber());
```
Check this code for some exemples [_Consumer.java](/src/main/java/com/amigoscode/functionalinterface/_Consumer.java) or see the video at [this point](https://youtu.be/VRpHdSFWGPs?t=2591)

## BiConsumers
Describes an interface that receive two ```Object``` and do anything with these.
```java
BiConsumer<Custumer, Boolean> greetCustumerBiConsumer =
    (custumer, showPhoneNumber) -> System.out.println("Hello " + custumer.getName() +
    ", thanks for registering your phone number " +
    (showPhoneNumber ? custumer.getPhoneNumber() : "********"));
```
Check this code for some exemples [_Consumer.java](/src/main/java/com/amigoscode/functionalinterface/_Consumer.java) or see the video at [this point](https://youtu.be/VRpHdSFWGPs?t=2893)

## Predicates
Describes an interface that receive an ```Object``` and return a boolean.
```java
Predicate<String> contaisNumber3 = phoneNumber -> phoneNumber.contains("3");
contaisNumber3.test("123") //true
```
Check this code for some exemples [_Predicate.java](/src/main/java/com/amigoscode/functionalinterface/_Predicate.java) or see the video at [this point](https://youtu.be/VRpHdSFWGPs?t=3129)

## Suppliers
Describes an interface that return a ```Object```.
```java
Supplier<String> getDBConnectionUrlSupplier =  () -> "jdbc://localhost:5432/users";
getDBConnectionUrlSupplier.get() // "jdbc://localhost:5432/users";
```
Check this code for some exemples [_Supplier.java](/src/main/java/com/amigoscode/functionalinterface/_Supplier.java) or see the video at [this point](https://youtu.be/VRpHdSFWGPs?t=3729)


## Conclusion
The functional programming defines a set of simple features that are critical to maintaining a good code structure 
according to this paradigm. The functions developed must be pure, without access to read or changing values of variables
in another scopes avoiding side effects. In summary, a function executed (n) times with the same parameters must present
the same result in all executions.
